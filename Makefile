# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ftothmur <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/04/22 14:23:27 by ftothmur          #+#    #+#              #
#    Updated: 2019/06/01 13:21:39 by ftothmur         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#-----------------------------------MISC---------------------------------------#
.PHONY:	all lib clean clean_lib fclean fclean_lib test re re_lib re_all

NAME =				test_gnl
OBJ =				$(SRC:.c=.o)
SPEED ?=			0
ifeq ($(SPEED), 0)
	MAIN = 			main
else
	MAIN = 			main_speed
endif
GNL =				get_next_line
LIB =				libft.a
vpath				%.c $(SRC_DIR)
#--------------------------------DIRECTORIES-----------------------------------#
LIB_DIR =			./libft/
HDR_LIB_DIR =		$(LIB_DIR)includes/
SRC_DIR = 			./
#--------------------------------SOURCE_FILES----------------------------------#
SRC =				$(GNL).c \
					$(MAIN).c
#-----------------------------COMPILER_OPTIONS---------------------------------#
DEBUG ?= 			0
ifeq ($(DEBUG), 0)
	CC_FLAGS = 		-Wall -Wextra -Werror
else
	CC_FLAGS = 		-Wall -Wextra -Werror -g
endif
I_FLAGS =			-I $(HDR_LIB_DIR)
LIB_FLAGS =			-L $(LIB_DIR) -lft
#-----------------------------------RULES--------------------------------------#
all:				$(NAME)

$(NAME):			$(LIB_DIR)$(LIB) $(OBJ)
					gcc -o $(NAME) $(MAIN).o $(GNL).o $(I_FLAGS) $(LIB_FLAGS)

$(LIB_DIR)$(LIB):	lib

lib:
					#make -C $(LIB_DIR) fclean &&
		   			make -C $(LIB_DIR)	

%.o :				%.c
					gcc -c $(CC_FLAGS) $^ $(I_FLAGS)

$(MAIN).o:			$(MAIN).c
					gcc $(CC_FLAGS) $(I_FLAGS) -o $(MAIN).o -c $(MAIN).c

$(GNL).o:			$(GNL).c
					gcc $(CC_FLAGS) $(I_FLAGS) -o $(GNL).o -c $(GNL).c

clean:
					/bin/rm -f *.o
			
clean_lib:
					make clean -C $(LIB_DIR)

fclean:				clean
					/bin/rm -f $(NAME)

fclean_lib:			
					make fclean -C $(LIB_DIR)

test:
					./test_gnl gnl1_1.txt gnl1_2.txt gnl1_3.txt gnl3_1.txt \
					   	gnl3_2.txt gnl5_1.txt gnl5_2.txt gnl5_3.txt gnl7_1.txt \
						gnl7_2.txt gnl7_3.txt gnl8_1.txt gnl8_2.txt \
						gnl8_3.txt gnl_to_free_or_not_to_free.txt gnl10.txt \
						gnl11_1.txt gnl11_2.txt 1_small_file.txt 2_lines.txt \
						4_10lines_no_n.txt 5_empty_lines.txt 6_empty_file.txt \
						ak.txt temp

voina1:
					time ./test_gnl voina.txt

voina2:
					time ./test_gnl voina-i-mir.txt

re:					fclean all

re_lib:				
					make re -C $(LIB_DIR)

re_all:				re_lib re
