/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftothmur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/21 14:37:11 by ftothmur          #+#    #+#             */
/*   Updated: 2019/05/23 23:10:52 by ftothmur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAIN_H
# define MAIN_H

/*
** 		<unistd.h>:		read, write, close, STDERR_FILENO, STDOUT_FILENO,
**						STDIN_FILENO
**		<sys/types.h>: 	ssize_t, size_t
**		<fcntl.h>:		open, file control options
*/
# include <unistd.h>
# include <sys/types.h>
# include <fcntl.h>
# include "get_next_line.h"

/*
** int		open(const char *filename, int access, ...);
** int		close(int fd);
*/

#endif
