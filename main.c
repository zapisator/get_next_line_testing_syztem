/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftothmur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/01 10:05:35 by ftothmur          #+#    #+#             */
/*   Updated: 2019/05/29 21:16:55 by ftothmur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"
#include <stdio.h>

/*
static char	*err_str(int error_no)
{
	char	*err_list[11];

	*(err_list + 0) = "Success";
	*(err_list + 1) = "No such file or directory";
	*(err_list + 2) = "Input/output error";
	*(err_list + 3) = "Argument list too long";
	*(err_list + 4) = "Bad file descriptor";
	*(err_list + 5) = "Cannot allocate memory";
	*(err_list + 6) = "Permission denied";
	*(err_list + 7) = "Invalid argument";
	*(err_list + 8) = "File too large";
	*(err_list + 9) = "No message of desired type";
	*(err_list + 10) = "Value too large for defined data type";
	return (*(err_list + error_no));
}

static int			ft_error(int error_no, int line, char *file)
{
	char	*err_message;

	if (error_no == SUCCESS)
		return (SUCCESS);
	err_message = err_str(error_no);
	ft_putstr_fd("\e[1;31mError! \e[3;36m", STDERR_FILENO);
	ft_putstr_fd(file, STDERR_FILENO);
	ft_putstr_fd("\e[m line ", STDERR_FILENO);
	ft_putnbr_fd(line, STDERR_FILENO);
	ft_putstr_fd(". ", STDERR_FILENO);
	ft_putendl_fd(err_message, STDERR_FILENO);
	return (FAILURE);
}
*/

int				main(int argc, char **argv)
{
	int			fd[FD_LIMIT];
	int			fd_used;
	char		**line;
	int			i;
	int			loop;

	fd_used = 0;
	if (argc > FD_LIMIT + 1)
	{
		if (argc == 1)
			return (ft_error(ENOENTRY, __LINE__, __FILE__));
		else
			return (ft_error(EINVALARG, __LINE__, __FILE__));
	}
	loop = 0;
	while (*(argv + 1))
	{
		printf("opened file name is: \e[0;36m%s\e[m\n", *(argv + 1));
		if ((*(fd + fd_used) = open(*(argv + 1), O_RDONLY)) == EOF)
			printf("\e[;32mError!\e[m EINOUT line %d\n", __LINE__);
		printf("opened fd = %d\n", *(fd + fd_used));
		if (!(line = (char **)ft_realloc(NULL, 0, sizeof(char *))))
			return (ft_error(ENOMEMORY, __LINE__, __FILE__));
		(void)printf("Beneath is the result:\n");
		while ((i = get_next_line(*(fd + fd_used), line)) > 0)
		{
			(void)printf("__________\n%s\n..........\n",
					line ? *line : "No *line");
			(void)printf("\e[1mgnl returned: %d\n\n\e[m", i);
			ft_memdel((void **)line);
		}
		(void)printf("__________\n%s\n..........\n",
					line ? *line : "No *line");
		(void)printf("\e[1mgnl returned: %d\n\e[m", i);
		(void)printf("==========\n");
		(void)printf("\n\e[1;32mgnl recall for the same fd, after file has been"
				" fully read:\e[m\n");
		i = get_next_line(*(fd + fd_used), line);
		(void)printf("__________\n%s\n..........\n",
					line ? *line : "No *line");
		(void)printf("\e[1mgnl returned: %d\n\e[m", i);
		ft_memdel((void **)&line);
		if (i == EOF)
			return (ft_error(ENOMSGS, __LINE__, __FILE__));
		argv++;
		if (!*(argv + 1))
			fd_used++;
	}
	fd_used--;
	ft_memdel((void **)&line);
	(void)printf("==========\n");
	while (fd_used >= 0)
	{
		(void)printf("fd_used = %d\n", fd_used + 1);
		printf("Before closing the file fd = %d\n\n", *(fd + fd_used));
		if ((close(*(fd + fd_used))) == EOF)
			return (ft_error(EINOUT, __LINE__, __FILE__));
		fd_used--;
	}
	return (0);
}
